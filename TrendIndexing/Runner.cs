﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using TrendIndexing.Data;
using TrendIndexing.Indexing;
using TrendIndexing.Processing;

namespace TrendIndexing
{
    public class Runner
    {
        private readonly Configuration _configuration;
        private Dictionary<int, string> _constantStrings;
        private Dictionary<int, int> _bundles;
        private Dictionary<int, int> _tracks;
        private Dictionary<int, int> _labels;
        private Dictionary<int, int> _trackGenres;
        private Dictionary<int, int> _bundleGenres;
        private readonly MsSqlReader _sqlReader;
        private readonly RedshiftReader _redshiftReader;
        private readonly IndexingModeEnum _indexingMode;
        private readonly IndexingTypeEnum _indexingType;
        public Runner(Configuration configuration, IndexingTypeEnum indexingType, IndexingModeEnum indexingMode)
        {
            _indexingType = indexingType;
            _indexingMode = indexingMode;
            _configuration = configuration;
            _sqlReader = new MsSqlReader(_configuration.MsSqlConnection);
            var redShiftConfiguration = new RedShiftConfiguration();
            redShiftConfiguration.RedshiftConnection = _configuration.RedshiftConnection;
            if (indexingType == IndexingTypeEnum.Analytics)
            {
                redShiftConfiguration.LocalFilePath = _configuration.AnalyticsLocalFilePath;
                redShiftConfiguration.S3Bucket = _configuration.AnalyticsS3Bucket;
            } else if (indexingType == IndexingTypeEnum.Trends)
            {
                redShiftConfiguration.LocalFilePath = _configuration.TrendLocalFilePath;
                redShiftConfiguration.S3Bucket = _configuration.TrendS3Bucket;
            }
            _redshiftReader = new RedshiftReader(redShiftConfiguration);
        }

        public async Task<bool> Run()
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            if (_indexingType == IndexingTypeEnum.Trends)
            {
                Log.Information("Indexing trends");
                await IndexAccounts();
                await IndexGenres();
                await IndexTrendRows(_indexingMode);
                if (_indexingMode == IndexingModeEnum.Incremental)
                {
                    DirectoryInfo localDirectory = new DirectoryInfo(_configuration.TrendLocalFilePath);
                    foreach (FileInfo file in localDirectory.GetFiles())
                    {
                        file.Delete();
                    }
                }
            } else if (_indexingType == IndexingTypeEnum.Analytics)
            {
                Log.Information("Indexing analytics");
                await IndexAnalyticsMonths(_indexingMode);
            }
            stopWatch.Stop();
            Log.Information("Indexing done. It took: " + stopWatch.Elapsed);
            return true;
        }

        private async Task<bool> IndexAnalyticsMonths(IndexingModeEnum indexingMode)
        {
            var indexer = new AnalyticsIndexer(_configuration.AnalyticsIndexName, _configuration.AnalyticsElasticSearchNode);
            //await indexer.AggregateTracks();
            Log.Information("Unloading Analytics");
            var fileList = await _redshiftReader.UnloadAnalyticsMonths();
            var numberOfThreads = 6;
            var sleepMs = 10000;
            if (fileList.Count() < numberOfThreads)
            {
                numberOfThreads = fileList.Count();
            }
            //ReadConstantStrings();
            var relatedData = ReadAnalyticsRelatedData();
            var threadList = new List<Task>();
            for (var threadNumber = 1; threadNumber <= numberOfThreads; threadNumber++)
            {
                Log.Information("Starting thread nr. " + threadNumber);
                var thread = Task.Factory.StartNew(() => indexer.IndexFiles(_constantStrings, relatedData, fileList).Result);
                threadList.Add(thread);
                Thread.Sleep(sleepMs);
            }
            foreach (var task in threadList)
            {
                task.Wait();
            }

            return true;
        }

        private async Task<bool> IndexTrendRows(IndexingModeEnum indexingMode)
        {
            var genreStruct = _sqlReader.GetGenreModels();

            var indexer = new Indexing.TrendIndexer(_configuration.TrendIndexName, _configuration.TrendsElasticSearchNode);
            long maxId = 0;
            var numberOfThreads = 12;
            var sleepMs = 10000;
            if (indexingMode == IndexingModeEnum.Incremental)
            {
                Log.Information("Getting maxid");
                maxId = await indexer.GetMaxTrendrowId();
                Log.Information("Done. Maxid: " + maxId);
                sleepMs = 2000;
            }
            Log.Information("Unloading Trendrows");
            var fileList = await _redshiftReader.UnloadTrendrows(maxId, indexingMode);
            if (fileList.Count() < numberOfThreads)
            {
                numberOfThreads = fileList.Count();
            }

            Log.Information("Done. " + fileList.Count() + " files is created");
            ReadConstantStrings();
            ReadTrendRelatedData();
            var threadList = new List<Task>();
            for (var threadNumber = 1; threadNumber <= numberOfThreads; threadNumber++)
            {
                Log.Information("Starting thread nr. " + threadNumber);
                var thread = Task.Factory.StartNew(() => indexer.IndexFiles(_constantStrings, _bundles, _tracks, _labels, _trackGenres, _bundleGenres, fileList).Result);
                threadList.Add(thread);
                Thread.Sleep(sleepMs);
            }
            foreach (var task in threadList)
            {
                task.Wait();
            }
            return true;
        }

        private void ReadConstantStrings()
        {
            Log.Information("Reading Constantstrings from DB");
            _constantStrings = _sqlReader.ReadConstantStrings();
            Log.Information("Done");
        }

        private AnalyticsRelatedData ReadAnalyticsRelatedData()
        {
            var relatedData = new AnalyticsRelatedData();
            Log.Information("Reading trackids");
            relatedData.TrackIds = _sqlReader.ReadTrackIds();
            Log.Information("Done");
            Log.Information("Reading bundleids");
            relatedData.BundleIds = _sqlReader.ReadBundleIds();
            Log.Information("Done");
            Log.Information("Reading bundles-artists");
            relatedData.BundleArtists = _sqlReader.ReadBundleArtists();
            Log.Information("Done");
            Log.Information("Reading tracks-artists");
            relatedData.TrackArtists = _sqlReader.ReadTrackArtists();
            Log.Information("Done");
            Log.Information("Reading artists-labels");
            relatedData.ArtistLabels = _sqlReader.ReadArtistLabels();
            Log.Information("Done");
            //Log.Information("Reading channel names");
            //relatedData.ChannelNames = _sqlReader.ReadChannelNames();
            //Log.Information("Done");
            //Log.Information("Reading channel identifiers");
            //relatedData.ChannelIdentifiers = _sqlReader.ReadChannelIdentifiers();
            //Log.Information("Done");

            return relatedData;
        }

        private void ReadTrendRelatedData()
        {
            Log.Information("Reading bundles");
            _bundles = _sqlReader.ReadBundleArtists();
            Log.Information("Done");
            Log.Information("Reading tracks");
            _tracks = _sqlReader.ReadTrackArtists();
            Log.Information("Done");
            Log.Information("Reading tracks");
            _labels = _sqlReader.ReadArtistLabels();
            Log.Information("Done");
            Log.Information("Reading genres");
            _trackGenres = _sqlReader.ReadTrackGenre();
            _bundleGenres = _sqlReader.ReadBundleGenre();
            Log.Information("Done");

        }

        private async Task<bool> IndexGenres()
        {
            Log.Information("Reading Genres");
            var genreModels = _sqlReader.GetGenreModels();
            Log.Information("Done");
            Log.Information("Indexing Genres");
            var genreIndexer = new GenreIndexer(_configuration.TrendsElasticSearchNode);
            await genreIndexer.IndexGenreModels(genreModels);
            Log.Information("Done");
            return true;

        }

        private async Task<bool> IndexAccounts()
        {
            Log.Information("Reading Accounts");
            var accountModels = _sqlReader.GetAccountModels();
            Log.Information("Done");
            Log.Information("Indexing accounts");
            var accountIndexer = new AccountIndexer(_configuration.TrendsElasticSearchNode);
            await accountIndexer.IndexAccountModels(accountModels);
            Log.Information("Done");
            return true;

        }

        public Dictionary<int, string> ReadConstantStringsFromFile()
        {
            var ucs = new Dictionary<int, string>();
            var lines = File.ReadAllLines("constantstrings.txt");
            foreach (var line in lines)
            {
                if (!string.IsNullOrWhiteSpace(line) && line.Contains(';'))
                {

                    var parts = line.Split(';');
                    ucs[Int32.Parse(parts[0])] = parts[1];
                }
            }

            return ucs;
        }

    }
}
