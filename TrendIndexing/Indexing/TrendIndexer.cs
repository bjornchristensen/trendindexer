﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataIndexing.Processing;
using Nest;
using Serilog;
using TrendIndexer.Indexing;
using TrendIndexing.Models;
using TrendIndexing.Processing;

namespace TrendIndexing.Indexing
{
    public class TrendIndexer
    {
        private readonly string _indexName;
        private static ElasticClient _client;
        private readonly IndexingOperations _indexingOperations;
        public TrendIndexer(string indexName, string elasticSearchNode)
        {
            _indexName = indexName;
            var settings = new ConnectionSettings(new Uri(elasticSearchNode)).DefaultFieldNameInferrer(x => x);
            _client = new ElasticClient(settings);
            _indexingOperations = new IndexingOperations(_client);
        }

        public async Task<BulkResponse> BulkIndexTrends(List<TrendRowWriteModel> trends, string indexName)
        {
            if (trends.Any())
            {
                var result = await _client.IndexManyAsync(trends, indexName);
                if (result.Errors)
                {
                    var errorString = string.Empty;
                    foreach (var error in result.ItemsWithErrors)
                    {
                        errorString += error.Error.Reason + Environment.NewLine;
                        errorString += error.Result + Environment.NewLine;
                    }
                    Log.Error("Bulkindexing went wrong. Errors: " + errorString);
                }
                return result;
            }
            return null;
        }

        public async Task<long> GetMaxTrendrowId()
        {
            var matchAllquery = new QueryContainer(new MatchAllQuery());
            var maxAggregation = new AggregationContainer()
            {
                Max = new MaxAggregation("MaxId", "Id")
            };
            var aggregations = new AggregationDictionary();
            aggregations.Add("MaxId", maxAggregation);
            ISearchRequest request = new SearchRequest(_indexName + "-*")
            {

                Query = matchAllquery,
                Aggregations = aggregations,
                Size = 0,
                IgnoreUnavailable = true
            };
            var result = await _client.SearchAsync<object>(request);
            var value = result.Aggregations.Max("MaxId")?.Value;
            if (value != null)
            {
                var maxId = (long)value;
                return maxId;
            }

            return 0;
        }

        public async Task<bool> IndexFiles(Dictionary<int, string> constantStrings,
            Dictionary<int, int> bundles,
            Dictionary<int, int> tracks,
            Dictionary<int, int> labels,
            Dictionary<int, int> trackGenres,
            Dictionary<int, int> bundleGenres,
            FileList fileList)
        {
            var end = false;
            while (!end)
            {
                var file = fileList.GetNextFile();
                if (file != null)
                {
                    Log.Information("File fetched. Files left: " + fileList.Count());
                    var trendRows = ReadTrendBatchFromFile(file);
                    await ProcessRows(trendRows, constantStrings, bundles, tracks, labels, trackGenres, bundleGenres);
                    //File.Delete(file);
                }
                else
                {
                    end = true;
                }
            }
            return true;
        }


        public async Task<bool> ProcessRows(Dictionary<string, List<TrendRowReadModel>> trendRows, Dictionary<int, string> constantStrings,
            Dictionary<int, int> bundles,
            Dictionary<int, int> tracks,
            Dictionary<int, int> labels,
            Dictionary<int, int> trackGenres,
            Dictionary<int, int> bundleGenres
            )
        {
            if (trendRows.Keys.Any())
            {
                foreach (var yearMonth in trendRows.Keys)
                {
                    var offSet = 0;
                    var batchSize = 15000;
                    var yearMonthTrends = trendRows[yearMonth];
                    if (yearMonthTrends.Any())
                    {
                        var yearMonthCount = yearMonthTrends.Count;
                        bool done = false;
                        var indexName = _indexName + "-" + yearMonth;
                        
                        var indexCreated = await _indexingOperations.CreateIndex<TrendRowWriteModel>(indexName,3, 0);
                        if (indexCreated)
                        {
                            Thread.Sleep(1000);
                            await _indexingOperations.UpdateMapping(indexName, "{\"properties\": { \"UserId\": { \"type\": \"keyword\",\"fields\": { \"hash\": { \"type\": \"murmur3\"}}}}}");
                        }
                        _indexingOperations.SetRefreshInterval(indexName, -1);
                        Thread.Sleep(1000);
                        while (!done)
                        {
                            if (yearMonthCount <= offSet + batchSize)
                            {
                                batchSize = (yearMonthCount) - offSet;
                                done = true;
                            }
                            var batch = yearMonthTrends.GetRange(offSet, batchSize);

                            var writeBatch = TrendRowMapper.MapStrings(batch, constantStrings, bundles, tracks, labels, trackGenres, bundleGenres);
                            await BulkIndexTrends(writeBatch, indexName);
                            offSet = offSet + batchSize;
                        }
                        _indexingOperations.RefreshIndex(indexName);
                    }
                }
            }

            return true;

        }


        private Dictionary<string, List<TrendRowReadModel>> ReadTrendBatchFromFile(string fullFilePath)
        {

            var trendRows = new Dictionary<string, List<TrendRowReadModel>>();
            var count = 0;
            using (var sr = new StreamReader(fullFilePath))
            {
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();
                    if (line != null)
                    {

                        var columns = line.Split("\t");

                        if (!string.IsNullOrWhiteSpace(columns[5]))
                        {
                            var dateOfSale = Convert.ToDateTime(columns[5]);
                            if (dateOfSale.Year > DateTime.UtcNow.Year || dateOfSale.Year < 2018)
                            {
                                continue;
                            }
                            var yearMonth = dateOfSale.Year + ".";
                            if (dateOfSale.Month < 10)
                            {
                                yearMonth = yearMonth + "0";
                            }
                            yearMonth = yearMonth + dateOfSale.Month;
                            //Try with bigger indexes and more shards
                            //yearMonth = yearMonth + dateOfSale.Month + ".";
                            //if (dateOfSale.Day < 10)
                            //{
                            //    yearMonth = yearMonth + "0";
                            //}
                            //yearMonth = yearMonth + dateOfSale.Day;
                            if (!trendRows.ContainsKey(yearMonth))
                            {
                                trendRows[yearMonth] = new List<TrendRowReadModel>();
                            }
                            trendRows[yearMonth].Add(
                                 new TrendRowReadModel()
                                 {
                                     Id = SafeLong(columns[0]),
                                     AccountId = SafeInt(columns[1]),
                                     SaleItemType = SafeInt(columns[2]),
                                     SaleItemId = SafeInt(columns[3]),
                                     UsageTimeInSec = SafeInt(columns[4]),
                                     DateOfSale = Convert.ToDateTime(columns[5]),
                                     Units = SafeInt(columns[6]),
                                     ShopStringId = SafeInt(columns[7]),
                                     OutletStringId = SafeInt(columns[8]),
                                     LabelStringId = SafeInt(columns[9]),
                                     ArtistStringId = SafeInt(columns[10]),
                                     TitleStringId = SafeInt(columns[11]),
                                     FormatStringId = SafeInt(columns[12]),
                                     EanStringId = SafeInt(columns[13]),
                                     IsrcStringId = SafeInt(columns[14]),
                                     TerritoryStringId = SafeInt(columns[15]),
                                     TitleVersionStringId = SafeInt(columns[16]),
                                     SaleDeliveryProductNameStringId = SafeInt(columns[17]),
                                     ShopSpecificIdStringId = SafeInt(columns[18]),
                                     ShopSpecificIdshopId = SafeInt(columns[19]),
                                     ShopSpecificIdtype = SafeInt(columns[20]),
                                     ZipCodeStringId = SafeInt(columns[21]),
                                     TrendId = SafeInt(columns[22]),
                                     ConsumerGenderStringId = SafeInt(columns[23]),
                                     ConsumerBirthyear = SafeInt(columns[24]),
                                     SourceStringId = SafeInt(columns[25]),
                                     DeviceTypeStringId = SafeInt(columns[26]),
                                     OsStringId = SafeInt(columns[27]),
                                     SourceUriStringId = SafeInt(columns[28]),
                                     ConsumerRegionStringId = SafeInt(columns[29]),
                                     SaleDeliveryServiceAccessStringId = SafeInt(columns[30]),
                                     SaleDeliveryFundingChannelStringId = SafeInt(columns[31]),
                                     SaleDeliveryServiceNameStringId = SafeInt(columns[32]),
                                     SourceTypeStringId = SafeInt(columns[33]),
                                     ReferralStringId = SafeInt(columns[34]),
                                     DateTimeOfSale = GetDateTimeOfSale(columns[35], columns[5]),
                                     PlayTimeRatio = GetPlayTimePercentages(columns[36]),
                                     PlayListNameStringId = SafeInt(columns[37]),
                                     UserId = columns[38]
                                 }
                             );
                        }
                    }
                    count++;
                }
                sr.Close();
            }
            return trendRows;
        }
        private decimal GetPlayTimePercentages(string playTimeRatio)
        {
            var ratio = SafeDecimal(playTimeRatio);
            if (ratio > 1)
            {
                return ratio;
            }
            decimal percentage = 0;
            if (ratio > 0)
            {
                percentage = Math.Round((ratio * 100), 2);
            }
            return percentage;
        }

        private static DateTime GetDateTimeOfSale(string dateTimeOfSale, string dateOfSale)
        {
            if (string.IsNullOrWhiteSpace(dateTimeOfSale))
            {
                dateTimeOfSale = dateOfSale;
            }

            return Convert.ToDateTime(dateTimeOfSale, CultureInfo.InvariantCulture);
        }
        private static int SafeInt(string toSave)
        {
            if (string.IsNullOrWhiteSpace(toSave) || toSave.Contains("."))
            {
                return 0;
            }

            return int.Parse(toSave);
        }
        private static long SafeLong(string toSave)
        {
            if (string.IsNullOrWhiteSpace(toSave) || toSave.Contains("."))
            {
                return 0;
            }

            return Int64.Parse(toSave);
        }

        private static decimal SafeDecimal(string toSave)
        {
            var returnValue = 0;
            if (string.IsNullOrWhiteSpace(toSave))
            {
                return returnValue;
            }
            try
            {
                return decimal.Parse(toSave, CultureInfo.InvariantCulture);
            }
            catch
            {
                return returnValue;
            }
        }
    }

}
