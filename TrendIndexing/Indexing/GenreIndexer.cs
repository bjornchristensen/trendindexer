﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Nest;
using TrendIndexer.Indexing;
using TrendIndexing.Models;

namespace TrendIndexing.Indexing
{
    public class GenreIndexer
    {
        private static ElasticClient _client;
        private readonly string _elasticSearchNode;
        private readonly IndexingOperations _indexingOperations;

        public GenreIndexer(string elasticSearchNode)
        {
            _elasticSearchNode = elasticSearchNode;
            var settings = new ConnectionSettings(new Uri(_elasticSearchNode)).DefaultFieldNameInferrer(x => x);
            _client = new ElasticClient(settings);
            _indexingOperations = new IndexingOperations(_client);
        }

        public async Task<bool> IndexGenreModels(List<GenreModel> genreModels)
        {
            await _indexingOperations.CreateIndex<GenreModel>("genres", 1, 1);
            var result = await _client.IndexManyAsync(genreModels, "genres");
            return result.IsValid;
        }

    }
}
