﻿namespace TrendIndexing.Indexing
{
    public class TrendRowMapping
    {

        public static string GetTrendRowMapping()
        {
            var mapping = "{\"properties\": {\"AccountId\": {\"type\": \"long\"},\"Artist\": {\"type\": \"keyword\"},\"ConsumerBirthyear\": {\"type\": \"long\"},\"ConsumerGender\": {\"type\": \"keyword\"},\"ConsumerRegion\": {\"type\": \"keyword\"},\"DateOfSale\": {\"type\": \"date\"},\"DeviceType\": {\"type\": \"keyword\"},\"Ean\": {\"type\": \"keyword\"},\"EanStringId\": {\"type\": \"long\"},\"Format\": {\"type\": \"keyword\"},\"Id\": {\"type\": \"long\"},\"Isrc\": {\"type\": \"keyword\"},\"IsrcStringId\": {\"type\": \"long\"},\"Label\": {\"type\": \"keyword\"},\"Os\": {\"type\": \"keyword\"},\"Outlet\": {\"type\": \"keyword\"},\"Referral\": {\"type\": \"keyword\"},\"SaleDeliveryFundingChannel\": {\"type\": \"keyword\"},\"SaleDeliveryProductName\": {\"type\": \"keyword\"},\"SaleDeliveryServiceAccess\": {\"type\": \"keyword\"},\"SaleDeliveryServiceName\": {\"type\": \"keyword\"},\"SaleItemId\": {\"type\": \"long\"},\"SaleItemType\": {\"type\": \"long\"},\"Shop\": {\"type\": \"keyword\"},\"ShopSpecificId\": {\"type\": \"keyword\"},\"ShopSpecificIdshopId\": {\"type\": \"long\"},\"ShopSpecificIdtype\": {\"type\": \"long\"},\"Source\": {\"type\": \"keyword\"},\"SourceType\": {\"type\": \"keyword\"},\"SourceUri\": {\"type\": \"keyword\"},\"Territory\": {\"type\": \"keyword\"},\"Title\": {\"type\": \"keyword\"},\"TitleVersion\": {\"type\": \"keyword\"},\"TrendId\": {\"type\": \"long\"},\"Units\": {\"type\": \"long\"},\"UsageTimeInSec\": {\"type\": \"long\"},\"ZipCode\": {\"type\": \"keyword\"},\"DateTimeOfSale\": {\"type\": \"date\"},\"PlayTimeRatio\": {\"type\": \"float\"},\"PlayListName\": {\"type\": \"keyword\"},\"UserId\": {\"type\": \"keyword\"},\"ArtistId\": {\"type\": \"long\"},\"LabelId\": {\"type\": \"long\"}}}";
            return mapping;
        }
    }
}
