﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Nest;

namespace TrendIndexer.Indexing
{
    public class IndexingOperations
    {
        private readonly ElasticClient _client;
        public IndexingOperations(ElasticClient elasticClient)
        {
            _client = elasticClient;
        }

        public async Task<bool> CreateIndex<T>(string indexName, int numberOfShards, int numberOfReplicas) where T : class
        {
            if (!_client.Indices.Exists(indexName).Exists)
            {
                var result = await _client.Indices.CreateAsync(indexName, c => c
                    .Settings(s => s
                        .NumberOfShards(numberOfShards)
                        .NumberOfReplicas(numberOfReplicas)
                    ).Map(ms => ms.AutoMap<T>())
                ); ;

                return result.IsValid;
            }
            return false;
        }
        public bool RefreshIndex(string indexName)
        {
            return _client.Indices.Refresh(indexName).IsValid;
        }

        public bool SetRefreshInterval(string indexName, int seconds)
        {
            var settings = new IndexSettings();
            if (seconds != -1)
            {
                settings.RefreshInterval = seconds + "s";
            }
            else
            {
                settings.RefreshInterval = seconds;
            }

            return _client.Indices.UpdateSettings(new UpdateIndexSettingsRequest(indexName) { IndexSettings = settings }).IsValid;
        }

        public List<string> GetAnalyticsIndicies(string startsWith)
        {
            var allIndicies = _client.Cat.Indices().Records;
            var analyticsIndicies = new List<string>();
            foreach (var index in allIndicies)
            {
                if (index.Index.StartsWith(startsWith))
                {
                    analyticsIndicies.Add(index.Index);
                }
            }

            return analyticsIndicies;
        }

        public bool DeleteIndicies(string startsWith)
        {
            var allIndicies = _client.Cat.Indices().Records;
            foreach (var index in allIndicies)
            {
                if (index.Index.StartsWith(startsWith))
                {
                    _client.Indices.Delete(index.Index);
                }
            }
            return true;
        }

        public ISearchRequest GetSearchRequest(List<QueryContainer> mustQueries,
            List<QueryContainer> mustNotQueries, AggregationDictionary aggregations, string indexName, string typeName)
        {
            var boolQuery = new BoolQuery
            {
                Must = mustQueries
            };
            if (mustNotQueries != null && mustNotQueries.Any())
            {
                boolQuery.MustNot = mustNotQueries;
            }
            ISearchRequest request = new SearchRequest(indexName)
            {
                Query = boolQuery,
                Aggregations = aggregations,
                Size = 0,
                IgnoreUnavailable = true
            };
            return request;
        }


        public async Task<HttpResponseMessage> UpdateMapping(string indexName, string mapping)
        {
            var client = new HttpClient();
            var totalUri = new Uri(_client.ConnectionSettings.ConnectionPool.Nodes.ToList()[0].Uri.AbsoluteUri  + indexName + "/_mapping");
            var httpContent = new StringContent(mapping, Encoding.UTF8, "application/json");
            var result = await client.PutAsync(totalUri, httpContent);
            return result;
        }


    }
}
