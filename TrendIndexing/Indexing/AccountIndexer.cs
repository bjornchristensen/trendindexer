﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Nest;
using TrendIndexer.Indexing;
using TrendIndexing.Models;

namespace TrendIndexing.Indexing
{
    public class AccountIndexer
    {
        private static ElasticClient _client;
        private readonly string _elasticSearchNode;
        private readonly IndexingOperations _indexingOperations;
        
        public AccountIndexer(string elasticSearchNode)
        {
            _elasticSearchNode = elasticSearchNode;
            var settings = new ConnectionSettings(new Uri(_elasticSearchNode)).DefaultFieldNameInferrer(x => x);
            _client = new ElasticClient(settings);
            _indexingOperations = new IndexingOperations(_client);
        }

        public async Task<bool> IndexAccountModels(List<AccountModel> accountModels)
        {
            await _indexingOperations.CreateIndex<AccountModel>("accounts", 6, 1);
            var result = await _client.IndexManyAsync(accountModels, "accounts");
            return result.IsValid;
        }

    }
}
