﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Nest;
using Serilog;
using TrendIndexer.Indexing;
using TrendIndexer.Models;
using TrendIndexing.Models;
using TrendIndexing.Processing;

namespace TrendIndexing.Indexing
{
    public class AnalyticsIndexer
    {
        private readonly string _indexName;
        private static ElasticClient _client;
        private readonly IndexingOperations _indexingOperations;

        public AnalyticsIndexer(string indexName, string elasticSearchNode)
        {
            _indexName = indexName;
            var settings = new ConnectionSettings(new Uri(elasticSearchNode)).DefaultFieldNameInferrer(x => x);
            _client = new ElasticClient(settings);
            _indexingOperations = new IndexingOperations(_client);
        }

        public async Task<BulkResponse> BulkIndexAnalytics(List<AnalyticsWriteModel> analytics, string indexName)
        {
            if (analytics.Any())
            {
                var result = await _client.IndexManyAsync(analytics, indexName);
                if (result.Errors)
                {
                    var errorString = "";
                    foreach (var error in result.ItemsWithErrors)
                    {
                        errorString += error?.Error?.Reason + Environment.NewLine;
                        errorString += error?.Result + Environment.NewLine;
                    }
                    Log.Error("Analytics bulkindexing went wrong. Errors: " + errorString);
                }
                return result;
            }

            return null;
        }

        public async Task<bool> IndexFiles(Dictionary<int, string> constantStrings,
        AnalyticsRelatedData relatedData,
            FileList fileList)
        {
            var end = false;
            while (!end)
            {
                var file = fileList.GetNextFile();
                if (file != null)
                {
                    var analyticsRows = ReadAnalyticsBatchFromFile(file);
                    await ProcessRows(analyticsRows, relatedData, constantStrings);
                    //File.Delete(file);
                }
                else
                {
                    end = true;
                }
            }
            return true;
        }

        public async Task<bool> ProcessRows(Dictionary<string, List<AnalyticsReadModel>> analyticsRows,
            AnalyticsRelatedData relatedData,
            Dictionary<int, string> constantStrings)
        {
            if (analyticsRows.Keys.Any())
            {
                foreach (var yearMonth in analyticsRows.Keys)
                {
                    var offSet = 0;
                    var batchSize = 15000;
                    var yearMonthTrends = analyticsRows[yearMonth];
                    if (yearMonthTrends.Any())
                    {
                        var yearMonthCount = yearMonthTrends.Count;
                        bool done = false;
                        var indexName = _indexName + "-" + yearMonth;
                        
                        var indexCreated = await _indexingOperations.CreateIndex<AnalyticsWriteModel>(indexName,1 ,0);
                        _indexingOperations.SetRefreshInterval(indexName, -1);
                        var sleepCount = 0;
                        while (!done)
                        {
                            if (yearMonthCount <= offSet + batchSize)
                            {
                                batchSize = (yearMonthCount) - offSet;
                                done = true;
                            }
                            var batch = yearMonthTrends.GetRange(offSet, batchSize);

                            var writeBatch = AnalyticsMonthMapper.MapStrings(batch,relatedData,constantStrings);
                            await BulkIndexAnalytics(writeBatch, indexName);
                            offSet = offSet + batchSize;
                            sleepCount++;
                            if (sleepCount > 1000000)
                            {
                                _indexingOperations.RefreshIndex(indexName);
                                sleepCount = 0;
                            }
                        }

                        _indexingOperations.RefreshIndex(indexName);
                    }
                    Thread.Sleep(1000);
                }
            }

            return true;

        }

        public async Task<bool> AggregateTracks()
        {
            _indexingOperations.DeleteIndicies("analyticstracks");
            var analyticsIndicies = _indexingOperations.GetAnalyticsIndicies("analytics-");
            foreach (var analyticIndex in analyticsIndicies)
            {
                var yearMonth = analyticIndex.Split("-")[1];
                var indexName = "analyticstracks-" + yearMonth;
                var queries = new List<QueryContainer>();
                queries.Add(GetSaleItemTypeQuery(1));
                var request = _indexingOperations.GetSearchRequest(queries, null, GetTrackListAggregation(),
                    analyticIndex, "analyticswritemodel");
                var debug = SerializeRequest(_client, (SearchRequest)request);
                var response = _client.Search<object>(request);
                var analyticsTracks = ReadTracks(response.Aggregations);
                var done = false;
                var offSet = 0;
                var batchSize = 15000;
                await _indexingOperations.CreateIndex<AnalyticsTrackModel>(indexName,1 ,2);
                while (!done)
                {
                    if (analyticsTracks.Count <= offSet + batchSize)
                    {
                        batchSize = (analyticsTracks.Count) - offSet;
                        done = true;
                    }
                    var batch = analyticsTracks.GetRange(offSet, batchSize);
                    var result = await _client.IndexManyAsync(batch, indexName);
                    if (!result.IsValid)
                    {
                        var errorString = "";
                        foreach (var error in result.ItemsWithErrors)
                        {
                            errorString += error.Error.Reason + Environment.NewLine;
                            errorString += error.Result + Environment.NewLine;
                        }
                        Log.Error("Analytics bulkindexing went wrong. Errors: " + errorString);
                    }
                    offSet = offSet + batchSize;
                }
            }

            return true;
        }
        public static string SerializeRequest(ElasticClient client, SearchRequest request)
        {
            var stream = new System.IO.MemoryStream();
            client.SourceSerializer.Serialize(request, stream);
            return Encoding.UTF8.GetString(stream.ToArray());
        }

        public List<AnalyticsTrackModel> ReadTracks(AggregateDictionary aggregate)
        {
            var trackList = new List<AnalyticsTrackModel>();
            var accountAggregation = aggregate.Terms("AccountId");
            foreach (var accountBucket in accountAggregation.Buckets)
            {
                var accountId = int.Parse(accountBucket.Key);
                var isrcAggregation = accountBucket.Terms("SaleItemId");
                if (isrcAggregation != null)
                {
                    foreach (var isrcBucked in isrcAggregation.Buckets)
                    {
                        foreach (var formatBucket in isrcBucked.Values)
                        {
                            var formatBucketValue =  (BucketAggregate) formatBucket;
                            foreach (var item in formatBucketValue.Items)
                            {
                                var analyticTrack = new AnalyticsTrackModel();
                                analyticTrack.SaleItemId = int.Parse(isrcBucked.Key);
                                analyticTrack.AccountId = accountId;

                                var formatValue = (KeyedBucket<object>)item;
                                var formatType = formatValue.Key.ToString();
                                analyticTrack.Format = formatType;
                                var count = (ValueAggregate)formatValue.Values.ToList().First();
                                if (count.Value != null)
                                {
                                    var formatCount = Convert.ToDouble(count.Value);
                                    analyticTrack.ReceivableAmount = formatCount;
                                }
                                trackList.Add(analyticTrack);
                            }
                        }
                    }
                }
            }

            return trackList;
        }
        private AnalyticsTrackModel ReadFormatListAggregations(AnalyticsTrackModel listItem, KeyedBucket<string> bucket)
        {
            foreach (var value in bucket.Values)
            {
                if (value.GetType() == typeof(BucketAggregate))
                {
                    var formatBuckets = (BucketAggregate)value;
                    foreach (var item in formatBuckets.Items)
                    {
                        var formatValue = (KeyedBucket<object>)item;
                        var formatType = formatValue.Key.ToString();
                        listItem.Format = formatType;
                        var count = (ValueAggregate)formatValue.Values.ToList().First();
                        if (count.Value != null)
                        {
                            var formatCount = Convert.ToDouble(count.Value);
                            listItem.ReceivableAmount = formatCount;
                        }
                    }
                }
            }
            return listItem;
        }


        private AggregationDictionary GetTrackListAggregation()
        {

            var unitAggregation = new AggregationDictionary();
            unitAggregation.Add("Units", new AggregationContainer()
            {
                Sum = new SumAggregation("Units", "ReceivableAmount")
            });
            var subAggregations = new AggregationDictionary();
            subAggregations.Add("Format", new AggregationContainer()
            {
                Terms = new TermsAggregation("Format") { Field = "Format", Size = 25 },
                Aggregations = unitAggregation
            });
            var termsAggregation = new AggregationDictionary
            {
                {
                    "SaleItemId",
                    new AggregationContainer()
                    {
                        Terms = new TermsAggregation("SaleItemId") {Field = "SaleItemId", Size = 500000},
                        Aggregations = subAggregations
                    }
                }
            };
            var accountAggregation = new AggregationDictionary
            {
                {
                    "AccountId",
                    new AggregationContainer()
                    {
                        Terms = new TermsAggregation("AccountId") {Field = "AccountId", Size = 500000},
                        Aggregations = termsAggregation
                    }
                }
            };
            return accountAggregation;
        }


        private QueryContainer GetSaleItemTypeQuery(int saleItemType)
        {
            return new QueryContainer(new TermQuery()
            {
                Field = "SaleItemType",
                Value = saleItemType
            });
        }



        private Dictionary<string, List<AnalyticsReadModel>> ReadAnalyticsBatchFromFile(string fullFilePath)
        {

            var analyticsRows = new Dictionary<string, List<AnalyticsReadModel>>();
            var count = 0;
            using (var sr = new StreamReader(fullFilePath))
            {
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();
                    if (line != null)
                    {
                        // try
                        // {
                            var columns = line.Split("\t");
                            if (!string.IsNullOrWhiteSpace(columns[3])) { 
                                var monthOfSaleInt = columns[3];
                                var year = monthOfSaleInt.Substring(0, 4);
                                var month = monthOfSaleInt.Substring(4, 2);

                                var yearMonth = year + "." + month;
                                if (!analyticsRows.ContainsKey(yearMonth))
                                {
                                    analyticsRows[yearMonth] = new List<AnalyticsReadModel>();
                                }
                                analyticsRows[yearMonth].Add(
                                     new AnalyticsReadModel()
                                     {
                                         Id = SafeInt(columns[0]),
                                         AccountId = SafeInt(columns[1]),
                                         Currency = columns[2],
                                         MonthOfSale = SafeInt(columns[3]),
                                         Format = columns[4],
                                         Territory = columns[5],
                                         Shop = columns[6],
                                         Outlet = columns[7],
                                         Ean = columns[8],
                                         Isrc = columns[9],
                                         SaleItemType = SafeInt(columns[10]),
                                         ReceivableAmount = SafeDecimal(columns[11])
                                     }
                                 );
                            }
                            
                        // } catch(Exception exception)
                        // {
                        //     Log.Error(exception, "Reading analytics file failed: " + exception.Message);
                        // }
                    }
                    count++;
                }
                sr.Close();
            }
            return analyticsRows;
        }

        private static int SafeInt(string toSave)
        {
            if (string.IsNullOrWhiteSpace(toSave) || toSave.Contains("."))
            {
                return 0;
            }

            return Int32.Parse(toSave);
        }
        private static decimal SafeDecimal(string toSave)
        {
            var returnValue = 0;
            if (string.IsNullOrWhiteSpace(toSave))
            {
                return returnValue;
            }
            try
            {
                return decimal.Parse(toSave, CultureInfo.InvariantCulture);
            }
            catch
            {
                return returnValue;
            }
        }

    }
}
