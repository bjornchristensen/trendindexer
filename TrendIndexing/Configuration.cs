﻿namespace TrendIndexing
{
    public class Configuration
    {
        public string RedshiftConnection { get; set; }
        public string MsSqlConnection { get; set; }
        public string TrendsElasticSearchNode { get; set; }
        public string AnalyticsElasticSearchNode { get; set; }
        public string SerilogNode { get; set; }
        public string TrendLocalFilePath { get; set; }
        public string AnalyticsLocalFilePath { get; set; }
        public string TrendS3Bucket { get; set; }
        public string AnalyticsS3Bucket { get; set; }
        public string AnalyticsIndexName { get; set; }
        public string TrendIndexName { get; set; }        
    }
}

