﻿using System;
using Nest;

namespace TrendIndexing.Models
{
    [ElasticsearchType(RelationName = "analyticswritemodel", IdProperty = "Id")]
    public class AnalyticsWriteModel
    {
        [Number(NumberType.Integer)]
        public int Id { get; set; }
        [Number(NumberType.Integer)]
        public int AccountId { get; set; }
        [Keyword]
        public string Currency { get; set; }
        [Number(NumberType.Integer)]
        public int MonthOfSale { get; set; }
        [Keyword]
        public string Territory { get; set; }
        [Keyword]
        public string Shop { get; set; }
        [Keyword]
        public string Format { get; set; }
        [Keyword]
        public string Outlet { get; set; }
        [Keyword]
        public string Ean { get; set; }
        [Keyword]
        public string Isrc { get; set; }
        [Number(NumberType.Integer)]
        public int SaleItemId { get; set; }
        [Number(NumberType.Integer)]
        public int SaleItemType { get; set; }
        [Number(NumberType.Integer)]
        public int ArtistId { get; set; }
        [Number(NumberType.Integer)]
        public int LabelId { get; set; }
        [Number(NumberType.Float)]
        public decimal ReceivableAmount { get; set; }
    }
}
