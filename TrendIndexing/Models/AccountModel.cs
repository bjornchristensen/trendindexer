﻿using System.Collections.Generic;

namespace TrendIndexing.Models
{
    public class AccountModel
    {
        public AccountModel()
        {
            AccountIds = new List<int>();
        }
        public int Id { get; set; }
        public List<int> AccountIds { get; set; }
    }
}
