﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;

namespace TrendIndexer.Models
{
    [ElasticsearchType(RelationName = "analyticsmodel")]
    public class AnalyticsTrackModel
    {
        [Number(NumberType.Integer)]
        public int AccountId { get; set; }
        [Number(NumberType.Integer)]
        public int SaleItemId { get; set; }
        [Keyword]
        public string Format { get; set; }
        [Number(NumberType.Float)]
        public double ReceivableAmount { get; set; }

    }
}
