﻿using System;

namespace TrendIndexing.Models
{
    public class TrendRowReadModel
    {
        public long Id { get; set; }
        public int AccountId { get; set; }
        public int SaleItemType { get; set; }
        public int SaleItemId { get; set; }
        public int UsageTimeInSec { get; set; }
        public DateTime DateOfSale { get; set; }
        public int Units { get; set; }
        public int ShopStringId { get; set; }
        public int OutletStringId { get; set; }
        public int LabelStringId { get; set; }
        public int ArtistStringId { get; set; }
        public int TitleStringId { get; set; }
        public int FormatStringId { get; set; }
        public int EanStringId { get; set; }
        public int IsrcStringId { get; set; }
        public int TerritoryStringId { get; set; }
        public int TitleVersionStringId { get; set; }
        public int SaleDeliveryProductNameStringId { get; set; }
        public int ShopSpecificIdStringId { get; set; }
        public string ShopSpecificId { get; set; }
        public int ShopSpecificIdshopId { get; set; }
        public int ShopSpecificIdtype { get; set; }
        public int ZipCodeStringId { get; set; }
        public int TrendId { get; set; }
        public int ConsumerGenderStringId { get; set; }
        public int ConsumerBirthyear { get; set; }
        public int SourceStringId { get; set; }
        public int DeviceTypeStringId { get; set; }
        public int OsStringId { get; set; }
        public int SourceUriStringId { get; set; }
        public int ConsumerRegionStringId { get; set; }
        public int SaleDeliveryServiceAccessStringId { get; set; }
        public int SaleDeliveryFundingChannelStringId { get; set; }
        public int SaleDeliveryServiceNameStringId { get; set; }
        public int SourceTypeStringId { get; set; }
        public int ReferralStringId { get; set; }
        public DateTime DateTimeOfSale { get; set; }
        public decimal PlayTimeRatio { get; set; }
        public int PlayListNameStringId { get; set; }
        public string UserId { get; set; }

    }
}
