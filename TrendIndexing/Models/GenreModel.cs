﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrendIndexing.Models
{
    public class GenreModel
    {
        public GenreModel()
        {
            GenreIds = new List<int>();
        }
        public int Id { get; set; }
        public List<int> GenreIds { get; set; }
    }
}
