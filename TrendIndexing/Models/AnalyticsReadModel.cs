﻿using System;

namespace TrendIndexing.Models
{
    public class AnalyticsReadModel
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public string Currency { get; set; }
        public int MonthOfSale { get; set; }
        public string Format { get; set; }
        public  string Territory { get; set; }
        public string Shop { get; set; }
        public string Outlet  { get; set; }
        public string Ean  { get; set; }
        public string Isrc  { get; set; }
        public int SaleItemType  { get; set; }
        public decimal ReceivableAmount  { get; set; }
    }
}
