﻿using System;
using Nest;

namespace TrendIndexing.Models
{
    [ElasticsearchType(RelationName = "trendrowwritemodel")]
    public class TrendRowWriteModel
    {
        [Number(NumberType.Long)]
        public long Id { get; set; }
        [Number(NumberType.Integer)]
        public int AccountId { get; set; }
        [Number(NumberType.Integer)]
        public int SaleItemType { get; set; }
        [Number(NumberType.Integer)]
        public int SaleItemId { get; set; }
        [Number(NumberType.Integer)]
        public int UsageTimeInSec { get; set; }
        [Date]
        public DateTime DateOfSale { get; set; }
        [Number(NumberType.Integer)]
        public int Units { get; set; }
        [Keyword]
        public string Shop { get; set; }
        [Keyword]
        public string Outlet { get; set; }
        [Keyword]
        public string Format { get; set; }
        [Keyword]
        public string Ean { get; set; }
        [Keyword]
        public string Isrc { get; set; }
        [Number(NumberType.Integer)]
        public int EanStringId { get; set; }
        [Number(NumberType.Integer)]
        public int IsrcStringId { get; set; }
        [Keyword]
        public string Territory { get; set; }
        [Keyword]
        public string Region { get; set; }
        [Keyword]
        public string TitleVersion { get; set; }
        [Keyword]
        public string ShopSpecificId { get; set; }
        [Number(NumberType.Integer)]
        public int ShopSpecificIdshopId { get; set; }
        [Number(NumberType.Integer)]
        public int ShopSpecificIdtype { get; set; }
        [Number(NumberType.Integer)]
        public int TrendId { get; set; }
        [Keyword]
        public string ConsumerGender { get; set; }
        [Number(NumberType.Integer)]
        public int ConsumerBirthyear { get; set; }
        [Keyword]
        public string Source { get; set; }
        [Keyword]
        public string DeviceType { get; set; }
        [Keyword]
        public string Os { get; set; }
        [Keyword]
        public string SourceUri { get; set; }
        [Keyword]
        public string ConsumerRegion { get; set; }
        [Keyword]
        public string SaleDeliveryServiceAccess { get; set; }
        [Keyword]
        public string SaleDeliveryFundingChannel { get; set; }
        [Keyword]
        public string SaleDeliveryServiceName { get; set; }
        [Keyword]
        public string SourceType { get; set; }
        [Date]
        public DateTime DateTimeOfSale { get; set; }
        [Number(NumberType.Float)]
        public decimal PlayTimeRatio { get; set; }
        [Keyword]
        public string PlayListName { get; set; }
        [Keyword]
        public string UserId { get; set; }
        
        [Number(NumberType.Integer)]
        public int ArtistId { get; set; }
        [Number(NumberType.Integer)]
        public int LabelId { get; set; }
        [Number(NumberType.Integer)]
        public int GenreId { get; set; }

    }
}
