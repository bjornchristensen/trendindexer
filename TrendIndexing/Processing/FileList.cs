﻿using System;
using System.Collections.Generic;

namespace TrendIndexing.Processing
{
    public class FileList
    {
        private readonly object _lock = new object();
        private readonly List<string> _fileList;

        public FileList(List<string> fileList)
        {
            this._fileList = fileList;
        }

        public int Count()
        {
            lock (_lock)
            {
                return _fileList.Count;
            }
        }

        public string GetNextFile()
        {
            lock (_lock)
            {
                if (_fileList.Count > 0)
                {
                        var file = _fileList[0];
                        _fileList.RemoveAt(0);
                        return file;
                }
                return null;
            }
        }
    }
}
