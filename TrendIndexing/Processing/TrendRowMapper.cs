using System;
using System.Collections.Generic;
using TrendIndexing.Models;


namespace DataIndexing.Processing
{
    static class TrendRowMapper
    {
        public static List<TrendRowWriteModel> MapStrings(List<TrendRowReadModel> trendRows, Dictionary<int, string> constantStrings,
            Dictionary<int, int> bundles,
            Dictionary<int, int> tracks,
            Dictionary<int, int> labels,
            Dictionary<int, int> trackGenres,
            Dictionary<int, int> bundleGenres
            )
        {
            var writeRows = new List<TrendRowWriteModel>();
            foreach (var row in trendRows)
            {
                try
                {
                    var artistId = 0;
                    var labelId = 0;
                    var genreId = 0;
                    if (row.SaleItemType == 1)
                    {
                        artistId = tracks[row.SaleItemId];
                        genreId = trackGenres[row.SaleItemId];
                    }
                    else if (row.SaleItemType == 2)
                    {
                        artistId = bundles[row.SaleItemId];
                        genreId = bundleGenres[row.SaleItemId];
                    }

                    if (artistId > 0)
                    {
                        labelId = labels[artistId];
                    }
                    var newRow = new TrendRowWriteModel
                    {
                        Id = row.Id,
                        AccountId = row.AccountId,
                        SaleItemType = row.SaleItemType,
                        SaleItemId = row.SaleItemId,
                        UsageTimeInSec = row.UsageTimeInSec,
                        DateOfSale = row.DateOfSale,
                        Units = row.Units,
                        ShopSpecificIdshopId = row.ShopSpecificIdshopId,
                        ShopSpecificIdtype = row.ShopSpecificIdtype,
                        TrendId = row.TrendId,
                        ConsumerBirthyear = row.ConsumerBirthyear,
                        Shop = constantStrings[row.ShopStringId].Trim(),
                        Outlet = constantStrings[row.OutletStringId].Trim(),
                        Format = constantStrings[row.FormatStringId].Trim(),
                        Ean = constantStrings[row.EanStringId].Trim(),
                        Isrc = constantStrings[row.IsrcStringId].Trim(),
                        EanStringId = row.EanStringId,
                        IsrcStringId = row.IsrcStringId,
                        Territory = constantStrings[row.TerritoryStringId].Trim().ToUpper(),
                        Region = constantStrings[row.ConsumerRegionStringId].Trim().ToUpper(),
                        TitleVersion = constantStrings[row.TitleVersionStringId].Trim(),
                        ShopSpecificId = constantStrings[row.ShopSpecificIdStringId].Trim(),
                        ConsumerGender = constantStrings[row.ConsumerGenderStringId].Trim(),
                        Source = constantStrings[row.SourceStringId].Trim(),
                        DeviceType = constantStrings[row.DeviceTypeStringId].Trim(),
                        Os = constantStrings[row.OsStringId].Trim(),
                        SourceUri = constantStrings[row.SourceUriStringId].Trim(),
                        ConsumerRegion = constantStrings[row.ConsumerRegionStringId].Trim(),
                        SaleDeliveryServiceAccess = constantStrings[row.SaleDeliveryServiceAccessStringId].Trim(),
                        SaleDeliveryFundingChannel = constantStrings[row.SaleDeliveryFundingChannelStringId].Trim(),
                        SaleDeliveryServiceName = constantStrings[row.SaleDeliveryServiceNameStringId].Trim(),
                        SourceType = constantStrings[row.SourceTypeStringId].Trim(),
                        DateTimeOfSale = row.DateTimeOfSale,
                        PlayTimeRatio = row.PlayTimeRatio,
                        PlayListName = constantStrings[row.PlayListNameStringId].Trim(),
                        UserId = row.UserId,
                        ArtistId = artistId,
                        LabelId = labelId,
                        GenreId = genreId
                    };

                    writeRows.Add(newRow);
                } catch(Exception exception)
                {
                    Serilog.Log.Logger.Error("Trendrow mapping went wrong", exception);
                }
            }

            return writeRows;
        }


    }
}
