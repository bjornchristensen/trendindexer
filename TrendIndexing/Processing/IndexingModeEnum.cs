﻿namespace TrendIndexing.Processing
{
    public enum IndexingModeEnum
    {
        Incremental = 0,
        Full = 1,
        Quarter = 2,
        Month = 3,
        Day = 4,
        None = 5,
        Files = 6
    } 
}
