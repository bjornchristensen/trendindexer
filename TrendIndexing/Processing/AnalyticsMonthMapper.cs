﻿using System;
using System.Collections.Generic;
using TrendIndexing.Models;

namespace TrendIndexing.Processing
{
    public class AnalyticsMonthMapper
    {
        public static List<AnalyticsWriteModel> MapStrings(List<AnalyticsReadModel> analyticsRows,
            AnalyticsRelatedData relatedData,
            Dictionary<int, string> constantStrings)
        {
            var writeRows = new List<AnalyticsWriteModel>();
            foreach (var row in analyticsRows)
            {
                var ean = row.Ean;
                var isrc = row.Isrc;
                var saleItemId = 0;
                var artistId = 0;
                var labelId = 0;

                var saleItemType = row.SaleItemType;
                try
                {
                    if (!string.IsNullOrWhiteSpace(isrc) && relatedData.TrackIds.ContainsKey(isrc.ToUpper()))
                    {
                        saleItemId = relatedData.TrackIds[isrc.ToUpper()];
                        artistId = relatedData.TrackArtists[saleItemId];
                    }
                    else if (!string.IsNullOrWhiteSpace(ean) && relatedData.BundleIds.ContainsKey(ean))
                    {
                        saleItemId = relatedData.BundleIds[ean];
                        artistId = relatedData.BundleArtists[saleItemId];
                    }

                    if (artistId > 0)
                    {
                        labelId = relatedData.ArtistLabels[artistId];
                    }
                    writeRows.Add(
                        new AnalyticsWriteModel()
                        {
                            Id = row.Id,
                            AccountId = row.AccountId,
                            Currency = row.Currency,
                            ReceivableAmount = row.ReceivableAmount,
                            Territory = row.Territory.Trim(),
                            Shop = row.Shop,
                            SaleItemType = saleItemType,
                            MonthOfSale = row.MonthOfSale,
                            Ean = ean,
                            Isrc = isrc,
                            Format = row.Format,
                            Outlet = row.Outlet,
                            SaleItemId = saleItemId,
                            ArtistId = artistId,
                            LabelId = labelId
                        }
                    );

                }
                catch (Exception e)
                {
                    Debugger.SendMail("Something went wrong: " + e.Message + "\n" + e.StackTrace);
                }
            }

            return writeRows;
        }

    }
}
