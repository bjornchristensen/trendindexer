﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TrendIndexing.Models;

namespace TrendIndexing.Data
{
    public class MsSqlReader
    {
        private readonly string _connectionString;
        private SqlConnection _connection;


        public MsSqlReader(string connectionString)
        {
            _connectionString = connectionString;
        }

        private void InitConnection()
        {
            _connection = new SqlConnection(_connectionString);
            _connection.Open();
        }

        private Dictionary<int, List<Int64>> ReadAccountTree()
        {
            var accountTree = new Dictionary<int, List<Int64>>();
            var sql = $@"SELECT accountid, a.[left], a.[right] FROM dbo.accounttree a";
            InitConnection();
            using (_connection)
            {
                var command = new SqlCommand(sql, _connection);
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        accountTree.Add(reader.GetInt32(0), new List<Int64>() { reader.GetInt64(1), reader.GetInt64(2) });
                    }
                }
            }
            _connection.Close();
            return accountTree;
        }

        public List<AccountModel> GetAccountModels()
        {
            var accountTree = ReadAccountTree();
            var accountModels = new List<AccountModel>();
            foreach (var id in accountTree.Keys)
            {
                var left = accountTree[id][0];
                var right = accountTree[id][1];
                var subAccounts = accountTree.Where(a => a.Value[0] >= left && a.Value[1] <= right).Select(i => i.Key).ToList();
                accountModels.Add(new AccountModel() { Id = id, AccountIds = subAccounts });
            }
            return accountModels;
        }

        public List<GenreModel> GetGenreModels()
        {
            var genres = new Dictionary<int, List<int>>();
            var sql = "SELECT GenreId, ParentGenreId FROM Genre";
            var genrePairs = new List<KeyValuePair<int, int>>();
            InitConnection();
            using (_connection)
            {
                var command = new SqlCommand(sql, _connection);
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var genreId = reader.GetInt32(0);
                        var parentId = reader.GetInt32(1);
                        var pair = new KeyValuePair<int, int>(genreId, parentId);
                        genrePairs.Add(pair);
                        genres.Add(genreId, new List<int>() { genreId });
                    }
                }
            }

            var subPairs = genrePairs.FindAll(p => p.Value > 0 && !genrePairs.FindAll(g => g.Value == p.Key).Any());

            foreach (var subPair in subPairs)
            {
                var current = subPair;
                while (current.Value > 0)
                {
                    var parent = genrePairs.FirstOrDefault(gp => gp.Key == current.Value);
                    if (!genres[parent.Key].FindAll(p => p == current.Key).Any())
                    {
                        genres[parent.Key].Add(current.Key);
                    }
                    current = parent;
                }
            }

            _connection.Close();
            var genreModels = new List<GenreModel>();
            foreach (var genre in genres.Keys)
            {
                genreModels.Add(
                    new GenreModel
                    {
                        Id = genre,
                        GenreIds = genres[genre]
                    }
                    );
            }
            return genreModels;
        }
        public Dictionary<int, string>ReadConstantStrings()
        {
            var uniqueConstantStrings = new Dictionary<int, string>();
            InitConnection();
            using (_connection)
            {
                var done = false;
                const int batchSize = 500000;
                var batchStart = 0;
                while (!done)
                {
                    var command = new SqlCommand($@"SELECT stringid, stringvalue FROM dbo.uniqueconstantstring WITH (NOLOCK) ORDER BY stringid OFFSET {batchStart} ROWS FETCH NEXT {batchSize} ROWS ONLY",
                        _connection);

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var id = reader.GetInt32(0);
                                var text = reader.GetString(1);
                                uniqueConstantStrings[id] = text;
                            }
                        }
                        else
                        {
                                done = true;
                        }
                    }
                    batchStart += batchSize;
                }
            }
            _connection.Close();
            uniqueConstantStrings[0] = "";
            return uniqueConstantStrings;
        }

        public Dictionary<string, int> ReadBundleIds()
        {
            var pairs = new Dictionary<string, int>();
            InitConnection();
            var sql = "";
            sql = $@"SELECT EAN, BundleID FROM dbo.Bundle ";
            using (_connection)
            {
                var command = new SqlCommand(sql, _connection);
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var eanObject = reader.GetValue(0);
                        if (eanObject != null)
                        {
                            var ean = eanObject.ToString().ToUpper();
                            var bundleId = reader.GetInt32(1);
                            pairs[ean] = bundleId;
                        }
                    }
                }
            }
            _connection.Close();
            return pairs;

        }

        public Dictionary<string, int> ReadTrackIds()
        {
            var pairs = new Dictionary<string, int>();
            InitConnection();
            var sql = "";
            sql = $@"SELECT ISRC, TrackID FROM dbo.Track ";
            using (_connection)
            {
                var command = new SqlCommand(sql, _connection);
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var isrcObject = reader.GetValue(0);
                        if (isrcObject != null)
                        {
                            var isrc = isrcObject.ToString().ToUpper();
                            var trackId = reader.GetInt32(1);
                            pairs[isrc] = trackId;
                        }
                    }
                }
            }
            _connection.Close();
            return pairs;

        }

        public Dictionary<int, Dictionary<string, int>> ReadChannelNames()
        {
            var channels = new Dictionary<int, Dictionary<string, int>>();
            InitConnection();
            var sql = "SELECT AccountId, ChannelName, ArtistId FROM Channel WHERE" +
                      " ArtistId is not null AND ArtistId > 0 AND AccountId is not null AND ChannelName is not null AND Active = 1";
            using (_connection)
            {
                var command = new SqlCommand(sql, _connection);
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var accountId = reader.GetInt32(0);
                        var channelName = reader.GetString(1);
                        var artistId = reader.GetInt32(2);
                        if (!channels.ContainsKey(accountId))
                        {
                            channels[accountId] = new Dictionary<string, int>();
                        }

                        channels[accountId][channelName] = artistId;
                    }
                }
            }
            _connection.Close();
            return channels;
    }

        public Dictionary<int, Dictionary<string, int>> ReadChannelIdentifiers()
        {
            var channels = new Dictionary<int, Dictionary<string, int>>();
            InitConnection();
            var sql = "SELECT AccountId, ChannelIdentifier, ArtistId FROM Channel WHERE" +
                      " ArtistId is not null AND ArtistId > 0 AND AccountId is not null AND ChannelIdentifier is not null AND Active = 1";
            using (_connection)
            {
                var command = new SqlCommand(sql, _connection);
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var accountId = reader.GetInt32(0);
                        var channelIdentifier = reader.GetString(1);
                        var artistId = reader.GetInt32(2);
                        if (!channels.ContainsKey(accountId))
                        {
                            channels[accountId] = new Dictionary<string, int>();
                        }

                        channels[accountId][channelIdentifier] = artistId;
                    }
                }
            }
            _connection.Close();
            return channels;
        }

        public Dictionary<int, int> ReadTrackGenre()
        {
            InitConnection();
            var trackGenres = new Dictionary<int, int>();
            var sql = @"SELECT [TrackId], [GenreID]
                        FROM[DanmarkMedia].[dbo].[Track] where GenreID is not null";
            using (_connection)
            {
                var command = new SqlCommand(sql, _connection);
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var trackId = reader.GetInt32(0);
                        var genreId = reader.GetInt32(1);

                        trackGenres.Add(trackId, genreId);
                    }
                }
            }
            _connection.Close();

            return trackGenres;
        }

        public Dictionary<int, int> ReadBundleGenre()
        {
            InitConnection();
            var bundleGenres = new Dictionary<int, int>();
            var sql = @"SELECT [BundleId],[GenreID] 
                        FROM[DanmarkMedia].[dbo].[Bundle]";
            using (_connection)
            {
                var command = new SqlCommand(sql, _connection);
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var bundleId = reader.GetInt32(0);
                        var genreId = reader.GetInt32(1);
                        bundleGenres.Add(bundleId, genreId);
                    }
                }
            }
            _connection.Close();

            return bundleGenres;
        }


        public Dictionary<int, int> ReadBundleArtists()
        {
            return ReadArtistId("bundle");
        }

        public Dictionary<int, int> ReadTrackArtists()
        {
            return ReadArtistId("track");
        }


        private Dictionary<int, int> ReadArtistId(string entityType)
        {
            var pairs = new Dictionary<int, int>();
            InitConnection();
            var sql = "";
            if (entityType.ToLower() == "bundle")
            {
                sql = $@"SELECT BundleID, ArtistID FROM dbo.Bundle ";
            }
            else
            {
                sql = $@"SELECT TrackID, ArtistID FROM dbo.Track ";
            }
            using (_connection)
            {
                var command = new SqlCommand(sql, _connection);
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = reader.GetInt32(0);
                        var artistId = reader.GetInt32(1);
                        pairs[id] = artistId;
                    }
                }
            }
            _connection.Close();
            return pairs;
        }

        public Dictionary<int, int> ReadArtistLabels()
        {
            var pairs = new Dictionary<int, int>();
            InitConnection();
            var sql = "";
            sql = $@"SELECT ArtistID, LabelID FROM dbo.Artist";
            using (_connection)
            {
                var command = new SqlCommand(sql, _connection);
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var artistId = reader.GetInt32(0);
                        var labelId = reader.GetInt32(1);
                        pairs[artistId] = labelId;
                    }
                }
            }
            _connection.Close();
            return pairs;
        }
    }
}
