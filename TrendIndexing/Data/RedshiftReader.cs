﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Npgsql;
using Serilog;
using TrendIndexing.Processing;

namespace TrendIndexing.Data
{
    public class RedshiftReader
    {
        private NpgsqlConnection _connection;
        private string _s3Directory;
        private RedShiftConfiguration _configuration;
        private readonly AmazonS3Client _s3Client = new AmazonS3Client("AKIAJDRM3NDFRVWXW4XA", "Yd0PANgETzZVTPdsyQbtC4t+J7S8Brm3jH+KTr8L",
            RegionEndpoint.EUWest1);

        public RedshiftReader(RedShiftConfiguration configuration)
        {
            _configuration = configuration;
        }
        private void InitConnection()
        {
            _connection = new NpgsqlConnection(_configuration.RedshiftConnection);
            _connection.Open();
        }
        public async Task<FileList> UnloadTrendrows(long fromId, IndexingModeEnum indexingMode)
        {
            //UnzipFiles();            
            //return new FileList(ReadFileNames(_configuration.LocalFilePath));
            if (indexingMode == IndexingModeEnum.Files)
            {
                return new FileList(ReadFileNames(_configuration.LocalFilePath));
            }
            var maxId = GetMaxTrendrowId();
            var whereClause = $" trendrowid > {fromId} ";
            if (indexingMode == IndexingModeEnum.Full)
            {
                whereClause = " dateofsale >= ''2018-01-01 00:00:00'' and dateofsale < ''2022-01-01'' ";
                //whereClause = " dateofsale >= ''2023-01-01 00:00:00'' AND isrcstringid = 10034661 ";                
            }
            else if (indexingMode == IndexingModeEnum.Quarter)
            {
                var fromDate = (DateTime.Now - TimeSpan.FromDays(90)).ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                whereClause = " dateofsale >= ''" + fromDate + "'' ";
            }
            else if (indexingMode == IndexingModeEnum.Month)
            {
                var fromDate = (DateTime.Now - TimeSpan.FromDays(30)).ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                whereClause = " dateofsale >= ''" + fromDate + "'' ";
            }
            else if (indexingMode == IndexingModeEnum.Day)
            {
                var fromDate = (DateTime.Now - TimeSpan.FromDays(1)).ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                whereClause = " dateofsale >= ''" + fromDate + "'' ";
            }
            else
            {
                if (fromId == 0)
                {
                    Log.Error("Trying to make incremental update but fromId == 0. Throwing exception");
                    throw new Exception("Trying to make incremental update but fromId == 0. Throwing exception");
                }
            }
            whereClause += " AND trendrowid <= " + maxId;
            _s3Directory = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            var sql = $@" unload('select trendrowid
                , accountid
                , saleitemtype
                , saleitemid
                , usagetimeinsec
                , dateofsale
                , units
                , shopstringid
                , outletstringid
                , labelstringid
                , artiststringid
                , titlestringid
                , formatstringid
                , eanstringid
                , isrcstringid
                , territorystringid
                , title_versionstringid
                , saledeliveryproductnamestringid
                , shopspecificidstringid
                , shopspecificidshopid
                , shopspecificidtype
                , zipcodestringid
                , trendid
                , consumergenderstringid
                , consumerbirthyear
                , sourcestringid
                , devicetypestringid
                , osstringid
                , sourceuristringid
                , consumerregionstringid
                , saledeliveryserviceaccessstringid
                , saledeliveryfundingchannelstringid
                , saledeliveryservicenamestringid
                , sourcetypestringid
                , referralstringid
                , datetimeofsale
                , playtimeratio
                , playlistnamestringid
                , userid 
            from dbo.trendrows_main t where {whereClause}
            and accountid is not null')  to 's3://{_configuration.S3Bucket}/{_s3Directory}/'
            credentials 'aws_access_key_id=AKIAJDRM3NDFRVWXW4XA;aws_secret_access_key=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'
            DELIMITER AS '\t' GZIP PARALlEL ON maxfilesize 150 mb;
            ";

            InitConnection();
            using (_connection)
            {
                var command = new NpgsqlCommand(sql, _connection);
                command.ExecuteNonQuery();
            }
            _connection.Close();
            return await FetchFiles();
        }

        public long GetMaxTrendrowId()
        {
            var sql = "SELECT max(trendrowid) FROM dev.dbo.trendrows_main;";
            long maxId = 0;
            InitConnection();
            using (_connection)
            {
                var command = new NpgsqlCommand(sql, _connection);
                using (var reader = command.ExecuteReader()) { 
                    reader.Read();
                    maxId = reader.GetInt64(0);
                }
            }
            _connection.Close();
            return maxId;
        }

        public async Task<FileList> UnloadAnalyticsMonths()
        {
            //return new FileList(ReadFileNames(_configuration.LocalFilePath));
            _s3Directory = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            var sql = $@"unload(' select 
                id
                , accountid
                , currency
                , monthofsale
                , format
                , territory
                , shop
                , outlet
                , ean
                , isrc
                , saleitemtype
                , receivableamount
            from dev.public.stats_detailed t where
            accountid is not null')  to 's3://{_configuration.S3Bucket}/{_s3Directory}/'
            credentials 'aws_access_key_id=AKIAJDRM3NDFRVWXW4XA;aws_secret_access_key=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'
            DELIMITER AS '\t' GZIP PARALlEL ON maxfilesize 150 mb;
            ";

            InitConnection();
            using (_connection)
            {
                var command = new NpgsqlCommand(sql, _connection);
                command.ExecuteNonQuery();
            }
            _connection.Close();
            return await FetchFiles();
            //return new FileList(ReadFileNames(_configuration.LocalFilePath));
        }

        public async Task<FileList> FetchFiles()
        {
            Log.Information("Fetching files from S3");
            if (!Directory.Exists(_configuration.LocalFilePath))
            {
                Directory.CreateDirectory(_configuration.LocalFilePath);
            }
            var existingFilesInLocalDir = Directory.GetFiles(_configuration.LocalFilePath);
            foreach (var existingFile in existingFilesInLocalDir)
            {
                File.Delete(existingFile);
            }
            using (var transferUtility = new TransferUtility(_s3Client))
            {
                var request = new TransferUtilityDownloadDirectoryRequest()
                {
                    BucketName = _configuration.S3Bucket,
                    S3Directory = _s3Directory,
                    LocalDirectory = _configuration.LocalFilePath
                    
                };
                transferUtility.DownloadDirectory(request);
            }
            await DeleteS3Directory();
            Log.Information("Done");
            UnzipFiles();
            return new FileList(ReadFileNames(_configuration.LocalFilePath));
            
        }
        public List<string> ReadFileNames(string filePath)
        {
            return Shuffle(Directory.GetFiles(filePath).ToList());
        }
        public static List<string> Shuffle(List<string> list)
        {
            Random rng = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                var value = list[k];
                list[k] = list[n];
                list[n] = value;
            }

            return list;
        }
        public Dictionary<int, string> ReadConstantStrings()
        {
            var uniqueConstantStrings = new Dictionary<int, string>();
            InitConnection();
            using (_connection)
            {
                var done = false;
                var batchSize = 10000;
                var batchStart = 0;
                var batchEnd = batchSize;
                var emptyCount = 0;
                while (!done)
                {
                    var command = new NpgsqlCommand($@"SELECT stringid, stringvalue FROM dbo.uniqueconstantstring WHERE stringid >= {batchStart} AND stringid <= {batchEnd} ",
                        _connection);
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var id = reader.GetInt32(0);
                                var text = reader.GetString(1);
                                uniqueConstantStrings[id] = text;

                            }
                        }
                        else
                        {
                            if (emptyCount > 50)
                            {
                                done = true;
                            }

                            emptyCount++;
                        }
                    }

                    batchStart += batchSize;
                    batchEnd += batchSize;
                }
            }
            _connection.Close();
            uniqueConstantStrings[0] = "";
            return uniqueConstantStrings;
        }
        private async Task<bool> DeleteS3Directory()
        {
            ListObjectsRequest listObjectsRequest = new ListObjectsRequest();
            String delimiter = "/";
            listObjectsRequest.BucketName = _configuration.S3Bucket;
            listObjectsRequest.Prefix = string.Concat(_s3Directory, delimiter);
            ListObjectsResponse listObjectsResponse = await _s3Client.ListObjectsAsync(listObjectsRequest);
            foreach (S3Object entry in listObjectsResponse.S3Objects)
            {
                await _s3Client.DeleteObjectAsync(new DeleteObjectRequest() {BucketName = _configuration.S3Bucket, Key = entry.Key});
            }
            return true;
        }
        public bool UnzipFiles()
        {
            Log.Information("Decompressing files");
            var dir = new DirectoryInfo(_configuration.LocalFilePath);
            var files = dir.GetFiles();
            foreach (var file in files)
            {
                Decompress(file);
                file.Delete();
            }
            Log.Information("Done");
            return true;
        }
        public static void Decompress(FileInfo fileToDecompress)
        {
            using (FileStream originalFileStream = fileToDecompress.OpenRead())
            {
                string currentFileName = fileToDecompress.FullName;
                string newFileName = currentFileName.Remove(currentFileName.Length - fileToDecompress.Extension.Length);

                using (FileStream decompressedFileStream = File.Create(newFileName))
                {
                    using (GZipStream decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
                    {
                        decompressionStream.CopyTo(decompressedFileStream);
                    }
                }
            }
        }
    }
}
