﻿namespace TrendIndexing
{
    public class RedShiftConfiguration
    {
        public string RedshiftConnection { get; set; }
        public string LocalFilePath { get; set; }
        public string S3Bucket { get; set; }
    }
}
