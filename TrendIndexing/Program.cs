﻿using System;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Formatting.Json;
using Serilog.Sinks;
using Serilog.Sinks.Elasticsearch;
using Serilog.Sinks.File;
using TrendIndexing.Indexing;
using TrendIndexing.Processing;

namespace TrendIndexing
{
    class Program
    {
        static void Main(string[] args)
        {
            Configuration configuration;
            try
            {
                configuration = LoadConfiguration();
            }
            catch (Exception exception)
            {
                Debugger.SendMail("Error reading configuration in trendindexer.\n" + exception.Message + "\n" + exception.StackTrace);
                throw;                                                                                                               
            }
            try
            {
                var esSinkOptions = new ElasticsearchSinkOptions(new Uri(configuration.SerilogNode))
                {
                    NumberOfShards = 1,
                    NumberOfReplicas = 0,
                    Period = TimeSpan.FromMilliseconds(1000)
                };
                var log = new LoggerConfiguration()
                    .WriteTo.Elasticsearch(esSinkOptions)
                    .WriteTo.Console()
                    .Enrich.WithProperty("Product", "Trendindexer")
                    .Enrich.WithProperty("Machine", Environment.MachineName)
                    .CreateLogger();
                Log.Logger = log;
                Log.Information("Logger initiated.");
            }
            catch (Exception exception)
            {
                //There is no logger here. Send mail
                Debugger.SendMail("Error creating logger in trendindexer.\n" + exception.Message + "\n" + exception.StackTrace);
                throw;
            }

            var indexingType = ResolveIndexType(args);
            var indexingMode = ResolveIndexMode(indexingType, args);
            try
            {
                Log.Information("Starting indexing");
                var runner = new Runner(configuration, indexingType, indexingMode);
                var result = runner.Run().Result;
            }
            catch (Exception exception)
            {
                Debugger.SendMail("Exception: " + exception.Message + "\nStacktrace: " + exception.StackTrace + "\nInnerException: " + exception.InnerException);
                Log.Error(exception, "TrendIndexer died");
                Log.CloseAndFlush();
            }
        }

        private static IndexingTypeEnum ResolveIndexType(string[] args)
        {
            if (args.Any() && args[0].ToLower() == "analytics")
            {
                return IndexingTypeEnum.Analytics;
            }
            return IndexingTypeEnum.Trends;
        }

        private static IndexingModeEnum ResolveIndexMode(IndexingTypeEnum indexingType, string[] args)
        {
            var index = 0;
            if (args.Length > 0 && (args[0].ToLower() == "trends" || args[0].ToLower() == "analytics"))
            {
                index = 1;
            }
            var indexingMode = IndexingModeEnum.Incremental;
            if (indexingType == IndexingTypeEnum.Trends)
            {
                if (args.Length > 0 && args[index].ToLower() == "full")
                {
                    indexingMode = IndexingModeEnum.Full;
                    Log.Information("Log started for full indexing");
                }
                else if (args.Length > 0 && args[index].ToLower() == "quarter")
                {
                    Log.Information("Log started for quarterly indexing");
                    indexingMode = IndexingModeEnum.Quarter;
                }
                else if (args.Length > 0 && args[index].ToLower() == "month")
                {
                    Log.Information("Log started for monthly indexing");
                    indexingMode = IndexingModeEnum.Month;
                }
                else if (args.Length > 0 && args[index].ToLower() == "day")
                {
                    Log.Information("Log started for day indexing");
                    indexingMode = IndexingModeEnum.Day;
                }
                else if (args.Length > 0 && args[index].ToLower() == "files")
                {
                    Log.Information("Log started for files indexing");
                    indexingMode = IndexingModeEnum.Files;
                }
                else
                {
                    Log.Information("Log started for incremental indexing");
                }
            }
            else
            {
                indexingMode = IndexingModeEnum.None;
            }

            return indexingMode;

        }

        private static Configuration LoadConfiguration()
        {
            try
            {
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

                IConfigurationRoot configuration = builder.Build();

                var localConfig = new Configuration();
                localConfig.RedshiftConnection = configuration.GetSection("RedshiftConnection").Value;
                localConfig.MsSqlConnection = configuration.GetSection("MsSqlConnection").Value;
                localConfig.TrendsElasticSearchNode = configuration.GetSection("TrendsElasticSearchNode").Value;
                localConfig.AnalyticsElasticSearchNode = configuration.GetSection("AnalyticsElasticSearchNode").Value;
                localConfig.SerilogNode = configuration.GetSection("SerilogNode").Value;
                localConfig.TrendLocalFilePath = configuration.GetSection("TrendLocalFilePath").Value;
                localConfig.TrendS3Bucket = configuration.GetSection("TrendS3Bucket").Value;
                localConfig.AnalyticsLocalFilePath = configuration.GetSection("AnalyticsLocalFilePath").Value;
                localConfig.AnalyticsS3Bucket = configuration.GetSection("AnalyticsS3Bucket").Value;
                localConfig.AnalyticsIndexName = configuration.GetSection("AnalyticsIndexName").Value;
                localConfig.TrendIndexName = configuration.GetSection("TrendIndexName").Value;
                return localConfig;
            }
            catch (Exception exception)
            {
                Debugger.SendMail("Exception: " + exception.Message + "\nStacktrace: " + exception.StackTrace + "\nInnerException: " + exception.InnerException);
                throw;
            }

        }
    }


}
