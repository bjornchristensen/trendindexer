﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrendIndexing
{
    public class AnalyticsRelatedData
    {
        public Dictionary<string, int> TrackIds { get; set; }
        public Dictionary<string, int> BundleIds { get; set; }
        public Dictionary<int, int> BundleArtists { get; set; }
        public Dictionary<int, int> TrackArtists { get; set; }
        public Dictionary<int, int> ArtistLabels{ get; set; }
        public Dictionary<int, Dictionary<string, int>> ChannelNames { get; set; }
        public Dictionary<int, Dictionary<string, int>> ChannelIdentifiers { get; set; }

    }
}
