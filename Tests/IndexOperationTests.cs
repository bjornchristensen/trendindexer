using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nest;
using TrendIndexer.Indexing;
using TrendIndexing.Models;

namespace Tests;

[TestClass]
public class IndexOperationTests
{
    [TestMethod]
    public void UpdateMappingTest()
    {
        var settings = new ConnectionSettings(new Uri("http://159.69.56.162:8080/")).DefaultFieldNameInferrer(x => x);
        var client = new ElasticClient(settings);
        var indexingOperations = new IndexingOperations(client);
        var createResult = indexingOperations.CreateIndex<TrendRowWriteModel>("trendrowtest", 1, 0).Result;
        var updateMappingResult = indexingOperations.UpdateMapping("trendrowtest", 
            "{\"properties\": {\"UserId\": {\"type\": \"keyword\",\"fields\": {\"hash\": {\"type\": \"murmur3\"}}}}}").Result;
    }
}

